module.exports = {
    FIELD_TYPES : {
        FOREIGN_KEY : 'foreignKey',
        UUID : 'uuid',
        STRING : 'string',
        REFERENCE : 'object',
        NUMBER : 'number',
        BOOLEAN : 'boolean',
        SHORT_ID : 'shortId',
        OBJECT : 'object',
        UNIQUE_NUMBER : 'uniqueNumber',
        SUB_DOCUMENT : 'subDocument'
    },
    RELATIONSHIPS : {
        HAS_MANY : 'hasMany',
        HAS_ONE : 'hasOne',
        BELONGS_TO : 'belongsTo'
    },
    MAX_GETS: 50,
    MAX_SAVES: 15,
};