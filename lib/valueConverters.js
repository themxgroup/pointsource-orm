var FIELD_TYPES = require('./ormConstants').FIELD_TYPES;

var boolean = {
    toRaw : function(value){
        if (value === true)
        {
            return 'Y';
        }
        return 'N';

    },
    fromRaw : function(value){
        if (value === 'Y'){
            return true;
        }
        return false;
    }
};

var CONVERTER_MAP = {};
CONVERTER_MAP[FIELD_TYPES.BOOLEAN] = boolean;

function get(type){
    return CONVERTER_MAP[type];
}

module.exports = {
    get : get
};