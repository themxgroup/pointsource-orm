var _ = require('lodash'),
    extend = require('util')._extend,
    JSONPath = require('jsonpath-plus'),
    FIELD_TYPES = require('./ormConstants').FIELD_TYPES,
    VALUE_CONVERTERS = require('./valueConverters'),
    RELATIONSHIPS = require('./ormConstants').RELATIONSHIPS,
    MAX_SAVES = require('./ormConstants').MAX_SAVES,
    generator = require('./generator'),
    uuid = require('node-uuid'),
    async = require('async'),
    shortid = require('shortid'),
    Q = require('q'),
    objectDiff = require('deep-diff'),
    logger = require('winston');

function constructDiffObjectFromDelta(deltas) {
    var object = diff = {};
    _.forEach(deltas, function (delta) {
        if (typeof delta.lhs === 'string' && typeof delta.rhs === 'string') {
            if (delta.lhs.trim() === delta.rhs.trim()) {
                return true;
            }
        }
        for (var i = 0; i < delta.path.length - 1; i++) {
            diff = object[delta.path[i]] = {};

        }
        diff[delta.path[i]] = delta.rhs;
    });
    return object;
}


var Instance = function (values) {
    this.values = [];

    this._isNull = function (value) {
        if (!value) {
            return true;
        }
        return this.Model.orm.options.spaceIsNull && value === ' ' || false;
    }

    /**
     * Creates a reference record
     * @param field
     * @param options
     * @param next
     */
    this.createRecord = function (field, options, next) {
        var self = this;
        var modelName = null;
        var fieldValue = field.value;
        if (Array.isArray(fieldValue) === false) {
            fieldValue = [fieldValue];
        }
        var target = field.relationship.target;
        var createdRecords = [];
        async.eachLimit(fieldValue, MAX_SAVES, function (value, cb) {
            if (typeof target === 'object') {
                // Polymorphism
                // Use the field value as the discriminator
                if (typeof target.oneOf === 'object') {
                    var discriminator = target.discriminator || field.fieldPath;
                    modelName = target.oneOf[value[discriminator]];
                }
            } else if (typeof target === 'function') {
                modelName = target(value);
            } else {
                modelName = target;
            }
            if (!modelName) {
                return cb('Model name cannot be null for field: ' + field.fieldPath);
            }
            var targetModel = self.Model.getModel(modelName);
            if (!targetModel) {
                return cb('Could not find target model: ' + modelName);
            }
            targetModel.create(value, options, function (err, record) {
                if (err) {
                    return cb(err);
                }
                createdRecords.push(record);
                cb(null, record);
            });
        }, function (err) {
            next(err, createdRecords);
        });
    }
};

Instance.prototype._setFromRaw = function (values, attributes, instanceValues, next) {
    var subDocument = false;
    if (next === undefined) {
        next = attributes;
        attributes = null;
        instanceValues = null;
    }

    if (!attributes) {
        attributes = this.Model.attributes;
    }
    if (!instanceValues) {
        instanceValues = this.values;
    }
    var self = this;
    async.forEach(attributes, function (attribute, callback) {
        var cloned = extend({}, attribute);
        var type = attribute.type;
        if (Array.isArray(attribute.attributes)) {
            var iValues = [];
            // Populate the subdocument of attributes:
            self._setFromRaw(values, attribute.attributes, iValues, function (err) {
                if (iValues.length > 0) {
                    cloned.value = iValues;
                    instanceValues.push(cloned);
                }
                callback();
            });
        } else {
            if (!attribute.column) {
                return callback()
            }

            var result = JSONPath({json: values, path: attribute.column});
            var value = result.length === 1 ? result[0] : null;

            Q.fcall(function () {
                if (typeof cloned.value === 'function') {
                    value = cloned.value(value);
                }
                var converter = cloned.converter;
                if (!converter) {
                    converter = VALUE_CONVERTERS.get(type);
                }
                if (converter && typeof converter.fromRaw === 'function') {
                    value = converter.fromRaw(value, values);
                }
                if (self._isNull(value)) {
                    return null;
                }
                return value;
            }).then(function (value) {
                if (value) {
                    cloned.value = value;
                    instanceValues.push(cloned);
                }
                callback();
            }, next);
        }

    }, next);

};

Instance.prototype._setFromDocument = function (documentValues, attributes, instanceValues, next) {
    var self = this;
    if (next === undefined) {
        next = attributes;
        attributes = null;
        instanceValues = null;
    }
    if (!attributes) {
        attributes = this.Model.attributes;
    }
    if (!instanceValues) {
        instanceValues = this.values;
    }
    async.forEach(attributes, function (attribute, callback) {
        var cloned = extend({}, attribute);
        if (cloned.ignoreOnDoc) {
            return callback();
        }
        var result = JSONPath({json: documentValues, path: attribute.objectPath || attribute.fieldPath});
        var value = result && result.length === 1 ? result[0] : null;
        if (Array.isArray(attribute.attributes)) {
            var iValues = [];
            self._setFromDocument(value, attribute.attributes, iValues, function (err) {
                if (err) {
                    return callback(err);
                }
                if (iValues.length > 0) {
                    cloned.value = iValues;
                    instanceValues.push(cloned);
                }
                callback();
            });
        } else {
            Q.fcall(function () {

                if (attribute.relationship && attribute.relationship.inherits) {
                    // If this is an extends, then the value needs to be the values object;
                    value = documentValues;
                }


                var type = attribute.type;
                // Generate any value
                if (!value && type === FIELD_TYPES.UUID) {
                    value = generator.uuid();
                } else if (!value && type === FIELD_TYPES.SHORT_ID) {
                    value = generator.shortId();
                } else if (!value && type === FIELD_TYPES.UNIQUE_NUMBER) {
                    value = generator.uniqueNumber();
                    if (attribute.prefix) {
                        value = attribute.prefix + value;
                    }
                } else if (typeof attribute.value === 'function') {
                    // If the value for the field is null, then use the complete object
                    value = attribute.value(value, documentValues);
                } else {
                    var converter = attribute.converter;
                    if (!converter) {
                        converter = VALUE_CONVERTERS.get(type);
                    }
                    if (converter && typeof converter.toRaw === 'function') {
                        return Q.when(converter.toRaw(value , documentValues));
                    }
                }
                return value;

            }).then(function (value) {
                if (value !== null && value !== undefined) {
                    cloned.value = value;
                    instanceValues.push(cloned);
                }
                callback();
            }, callback)
        }

    }, next);
};

Instance.prototype.set = function (values, options, next) {
    if (next === undefined) {
        next = options;
        options = null;
    }
    options = options || {};
    // Build an instance from the column mappings
    if (options.raw) {
        this._setFromRaw(values, next);
    } else {
        // This is a json document, build up the attributes from json objects
        this._setFromDocument(values, next);
    }

};

Instance.prototype.getValue = function (key) {
    var attribute = this.values.find(function (item) {
        return item.fieldPath === key;
    });
    return attribute && attribute.value || null;
};

Instance.prototype.setValue = function (key, value) {
    if (value !== null &&
        value !== undefined &&
        (!Array.isArray(value) || value.length > 0)) {
        var attribute = this.Model.getAttribute(key);
        if (attribute) {
            var cloned = _.extend({}, attribute);
            cloned.value = value;
            this.values.push(cloned);
        }
    }
};


function setValueByPath(receiver, key, value) {
    var result = object = {};
    var arr = key.split('.');
    for (var i = 0; i < arr.length - 1; i++) {
        object = object[arr[i]] = {};
    }
    object[arr[arr.length - 1]] = value;
    if (arr.length > 1) {
        _.merge(receiver, result);
    } else {
        _.extend(receiver, result);
    }
}

/**
 * Builds a json object from the set values
 */
Instance.prototype.toJson = function () {
    var json = {};
    this.values.forEach(function (item) {

        if (!item.ignore && (item.value !== null || !item.ignoreIfNull)) {
            if (item.type === FIELD_TYPES.SUB_DOCUMENT) {
                if (item.value) {
                    var subJson = json[item.fieldPath] = {};
                    item.value.forEach(function (subItem) {
                        setValueByPath(subJson, subItem.objectPath || subItem.fieldPath, subItem.value)
                    });
                }
            } else {
                setValueByPath(json, item.objectPath || item.fieldPath, item.value);
            }
        }
    });
    return json;
};

/**
 * Binds source values to target values.  If a relationship is specified, the
 * foriegn key attribute is inspected to determine which way the values are injected
 * @param source
 * @param target
 * @param field
 */

Instance.prototype.bindForeignKeyValues = function (field) {

    var foreignKey = field.relationship.foreignKey;
    if (Array.isArray(foreignKey) === false) {
        foreignKey = [foreignKey];
    }
    var self = this;
    _.forEach(foreignKey, function (fk) {
        var sourceKey = null;
        var targetKey = null;
        if (typeof foreignKey === 'object') {
            // Designated source/target
            sourceKey = fk.source;
            targetKey = fk.target;
        }
        var sourceValue = self.getValue(sourceKey);
        var fieldValue = field.value;
        if (Array.isArray(fieldValue) === false) {
            fieldValue = [fieldValue];
        }
        fieldValue.forEach(function (item) {
            item[targetKey] = sourceValue;
        });
    });
};

Instance.prototype.wrapAndCall = function (fn, options, next) {
    var self = this;
    if (next === undefined) {
        next = options;
        options = {};
    }

    options.include = options.include || {};

    if (options.connection === undefined) {
        // wrap the next so we can make a transaction
        self.Model.orm.runWithConnection(function (connection, done) {
            options.connection = connection;
            fn.call(self, options, done);
        }, next);

    } else {
        fn.call(self, options, next);
    }
}

Instance.prototype.create = function (options, next) {
    this.wrapAndCall(this._create, options, next);
};

Instance.prototype._create = function (options, next) {
    var self = this;
    var record = {};
    var belongToRefs = [];
    var hasRefs = [];

    // Sets up a json object that contains the column mappings to values
    _.forEach(this.values, function (field) {
        var value = field.value;
        var column = field.column;
        if (field.relationship) {
            var relType = field.relationship.type;
            if (relType === RELATIONSHIPS.BELONGS_TO) {
                if (typeof value === 'object') {
                    // This needs to be created first, then get the
                    belongToRefs.push(field);
                } else {
                    // If the value is not an object then it is assumed
                    // that the value has been injected
                    record[column] = value;
                }

            } else {
                hasRefs.push(field);
            }
        }
        if (column && value !== null) {
            record[column] = value;
        }
    });
    async.eachSeries(belongToRefs, function (field, cb) {
        self.createRecord(field, options, function (err, foreignRecords) {
            if (err) {
                return cb(err);
            }
            // Inject the foreign key here..
            // Take the foreign key from the target, and insert it into the source
            if (foreignRecords.length === 1) {
                var foreignKey = field.relationship.foreignKey;
                var keyValue = foreignRecords[0].getValue(foreignKey);

                record[field.column] = keyValue;
            } else {
                return cb('There was an unexpected number of foreign records created.  BELONGS_TO_MANY not supported');
            }
            cb();

        });
    }, function (err, result) {
        if (err) {
            return next(err);
        }
        options.connection.insert(self.Model.getTableName(), [record], function (err, results) {
            if (err) {
                return next(err);
            }
            // Insert the record
            async.eachSeries(hasRefs, function (field, cb) {
                // Inject foreign key values into the foreign records
                self.bindForeignKeyValues(field);
                self.createRecord(field, options, cb);
            }, function (err, results) {
                next(err, self);
            });
        });

    });

}

Instance.prototype._upsert = function (options, next) {
    var self = this;
    // Do a query to see if this record needs to be updated or inserted
    this.find(options, function (err, primaryKeyQuery, record) {
        if (err) {
            return next(err);
        }
        if (record && primaryKeyQuery) {
            return self._update(options, primaryKeyQuery, record, next);
        }
        self._create(options, next);
    });
}

Instance.prototype.upsert = function (options, next) {
    this.wrapAndCall(this._upsert, options, next);
};

/**
 * This is a simple pull all the data out of the values to form a flat column mapping.
 * Ignores primary ke attributes as this is for an update.  The primary key attributes
 * are used in the where clause
 * @param values
 * @returns {{}}
 */
function convertValuesToUpdateColumnMap(values) {
    /**
     * TODO: Reference updating may be needed.  at the time of this writing,
     * it is not.
     * @type {{}}
     */
    var record = {};
    _.forEach(values, function (field) {
        if (field.type === FIELD_TYPES.SUB_DOCUMENT) {
            record = _.merge(record, convertValuesToUpdateColumnMap(field.value));
        }
        if (!field.primaryKey) {
            var value = field.value;
            var column = field.column;
            if (column && value !== null) {
                record[column] = value;
            }
        }
    });
    return record;
}

Instance.prototype.find = function (options, next) {
    var key = this.Model.getPrimaryKey();
    if (!key) {
        return next('Cannot update without primary key');
    }

    var self = this;
    var primKeyQuery = {};
    _.forEach(key, function (k) {
        var value = self.getValue(k.fieldPath);
        if (value) {
            primKeyQuery[k.fieldPath] = value;
        }
    });
    if (Object.keys(primKeyQuery).length === 0) {
        return next('Primary key not defined for model: ' + this.Model.getModelName());
    }
    this.Model.findOne(primKeyQuery, options, function (err, records) {
        if (err) {
            return next(err);
        }
        if (records.length === 1) {
            return next(null, primKeyQuery, records[0]);
        }
        next();
    });
}

Instance.prototype._update = function (options, primaryKeyQuery, oldRecord, next) {
    // Do a primary key search to see if the record exists.
    // If not, do a create instead.
    var self = this;
    // We should do a compare with the record and the json
    if (oldRecord && primaryKeyQuery) {
        // Need to convert the query that has key/value according to the document model
        // into one that is native and uses the column names but same values.
        var nativeQuery = _.mapKeys(primaryKeyQuery, function (value, key) {
            return self.Model.getColumn(key);
        });

        // Filter out only the diff values
        var filterDiffAttributes = function (values, targetObj) {
            if (!targetObj) {
                return [];
            }
            var diffValues = values.filter(function (field) {
                if (field.attributes) {
                    field.value = filterDiffAttributes(field.value, targetObj[field.fieldPath]);
                    return field.value.length > 0;
                }
                return targetObj[field.fieldPath] !== undefined;
            });
            return diffValues;
        }


        //Construct a diff object between the incoming changes and the current record
        //This is a waste.  I am rebuilding the old json as an incoming document so that any
        //triggers and value handlers that transform the data are accounted for when computing the
        //diff. If the data wasn't so crappy, this wouldn't be necessary.
        self.Model.getModel(self.Model.getModelName()).build(oldRecord, function (err, instance) {
            if (err) {
                return next(err);
            }
            var diff = constructDiffObjectFromDelta(objectDiff(instance.toJson(), self.toJson()));
            if (_.keys(diff).length === 0)
            {
                logger.debug('No differences found between the source record and the target record, ignoring update.. ');
                return next();
            }
            var diffValues = filterDiffAttributes(_.cloneDeep(self.values), diff);
            if (!diffValues || diffValues.length === 0) {
                logger.debug('No differences found between the source record and the target record, ignoring update.. ');
                return next();
            }

            // Create the column mapping
            var columnMap = convertValuesToUpdateColumnMap(diffValues);
            // Finally, update the record.
            return options.connection.update(self.Model.getTableName(), nativeQuery, columnMap, next);
        });


    }
};

module.exports = Instance;