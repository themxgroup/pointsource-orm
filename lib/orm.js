var Model = require('./model'),
    path = require('path'),
    file = require('file'),
    _ = require('lodash'),
    _databaseProvider,
    logger = require('winston');

logger.level = 'debug';

function ORM(){
    this.options = {};
    this.models = {};

    var self = this;

    this.init = function(databaseProvider, scanDir, callback) {
        _databaseProvider = databaseProvider;
        this.scan(scanDir, callback);
    };

    this.scan = function(dir, callback){
        logger.debug('Scanning dir: ' + dir);
        file.walkSync(dir, function (dirPath, dirs, files) {
            files.forEach(function (name) {
                processFile(path.join(dirPath, name));
            });
        });
        callback();
    };

    var processFile = function(file, callback) {
        var module = require(file);
        if (module.name && module.schema && module.tableName)
        {
            createModel(module);
            logger.info('Registered model: ' + module.name);
        }
    };

    this.runWithConnection = function(runnable, next){
        _databaseProvider.runWithConnection(runnable, next);
    };

    var createModel = function(schema){
        var model = new Model(self , schema);
        self.models[schema.name] = model;
        return model;
    };

    this.getModel = function(name){
        return this.models[name];
    };

    this.getModelByTableName = function(tableName){
        return _.values(this.models).find(function(model){
                return model.getTableName().toLowerCase() === tableName.toLowerCase();
        });
    }
}

var INSTANCE = new ORM();

module.exports = INSTANCE;