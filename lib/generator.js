var shortid = require('shortid'),
    uuid = require('node-uuid');

var ALPHABET = '0123456789';
module.exports.uniqueNumber = function(){
    var ID_LENGTH = 10;
    var rtn = '';
    for (var i = 0; i < ID_LENGTH; i++) {
        rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
    }
    return rtn;
};

//var uniqueNumber = {
//    previous : 0
//};
//
//module.exports.uniqueNumber = function(){
//    var date = Date.now();
//
//    // If created at same millisecond as previous
//    if (date <= uniqueNumber.previous) {
//        date = ++uniqueNumber.previous;
//    } else {
//        uniqueNumber.previous = date;
//    }
//    return date;
//};


module.exports.uuid = function(){
    return uuid.v4();
};

module.exports.shortId = function(){
    return shortid.generate();
};