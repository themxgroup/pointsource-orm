function Connection() {
};

Connection.prototype.commit = function(next){
    throw new Error('Commit not implemented');
};

Connection.prototype.rollback = function (next) {
    throw new Error('Rollback not implemented');
};

Connection.prototype.release = function (next) {
    throw new Error('Release not implemented');
};

Connection.prototype.escape = function(sql){
    throw new Error('Escape not implemented');
};

Connection.prototype.execute = function (options, retryCnt, next) {
    throw new Error('Execute not implemented');
};

Connection.prototype.insert = function (tableName, record, next) {
    throw new Error('Insert not implemented');
};

Connection.prototype.select = function (tableName, options, next) {
    throw new Error('Select not implemented');
};

Connection.prototype.format = function (sql, args) {
    throw new Error('Format not implemented');
};

module.exports = Connection;