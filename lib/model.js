var extend = require('util')._extend,
    util = require('util'),
    Instance = require('./instance'),
    async = require('async'),
    RELATIONSHIPS = require('./ormConstants').RELATIONSHIPS,
    FIELD_TYPES = require('./ormConstants').FIELD_TYPES,
    MAX_GETS = require('./ormConstants').MAX_GETS,
    _ = require('lodash');


function ORMModel(orm, pack) {
    this._package = pack;
    this.orm = orm;
    this.attributes = [];
    this.inherits = null;
    this.Instance = function () {
        Instance.apply(this, arguments);
    };

    this.getModelName = function(){
        return this._package.name;
    };

    this.getPackage = function(){
        return this._package;
    };

    util.inherits(this.Instance, Instance);
    this.Instance.prototype.Model = this;

    var self = this;

    function processAttribute(value, fieldPath, attributeList){
        var attribute = extend({}, value);
        attribute.fieldPath = fieldPath;
        attributeList.push(attribute);
        var relationship = attribute.relationship;
        if (relationship && relationship.inherits) {
            self.inherits = {
                schema: relationship.target,
                join: {
                    base: relationship.foreignKey,
                    subclass: attribute.fieldPath
                }
            }
        }
        if (attribute.type === FIELD_TYPES.SUB_DOCUMENT)
        {
            attribute.attributes = [];
            // Process the subdocument attributes
            _.forEach(attribute.document, function(subV, subPath){
                processAttribute(subV, subPath, attribute.attributes);
            });
        }
    }

    _.forEach(this._package.schema, function (value, fieldPath) {
        processAttribute(value, fieldPath, self.attributes);
    });

    this.getTableName = function () {
        return this._package.tableName;
    };

    this.getAttributes = function () {
        return this.attributes;
    };

    this.getModel = function (name) {
        return this.orm.models[name];
    };

    this.build = function (values, options, next) {
        if (next === undefined)
        {
            next = options;
            options = null;
        }
        options = options || {};

        var instance = new this.Instance();
        instance.set(values, options, function(err){
            next(err, instance);
        });
    };

    this.upsert = function(values, options, next){
        if (next === undefined) {
            next = options;
            options = {};
        }

        this.build(values, options, function(err, instance){
            if (err){
                return next(err);
            }
            instance.upsert(options, next);
        });
    };

    this.getAttribute = function (key) {
        return self.attributes.find(function (item) {
            return item.fieldPath === key;
        });
    };

    this.getColumn = function (key) {
        var column = null;
        var attribute = self.getAttribute(key);

        if (attribute) {
            column = attribute.column;
        }
        return column;
    };

    this.getInherits = function () {
        return this.inherits;
    };


    this.getKeyForColumn = function (column) {
        var attribute = self.attributes.find(function (item) {
            return item.column === column;
        });
        return attribute && attribute.fieldPath || null;
    };

    /**
     * This is not recommended to use.  There can be multiple primary keys
     * @Deprecated
     * @see getPrimaryKey
     * @returns {*}
     */
    this.findPrimaryKeyAttribute = function () {
        return self.attributes.find(function (item) {
            return item.primaryKey === true;
        });
    };

    this.getPrimaryKey = function () {
        return self.attributes.filter(function (item) {
            return item.primaryKey === true;
        });
    };

    this.create = function (values, options, next) {
        if (next === undefined) {
            next = options;
            options = {};
        }

        this.build(values, options, function(err, instance){
            if (err){
                return next(err);
            }
            instance.create(options, next);
        });


    };

    var rawRow = function (record, metadata) {
        var json = {};
        _.forEach(metadata, function (c, idx) {
            json[c.name] = record[idx];
        });
        return json;
    };


    var resolveModelReferences = function(model, instance, options, next){
        var tasks = [];
        // Build a fake instance for the base model
        model.build({}, function(err, baseInstance){
            _.forEach(model.attributes, function(a){
                var relationship = a.relationship;
                if (relationship && !a.primaryKey)
                {
                    tasks.push(function(cb){
                        resolveReference(a, baseInstance, options, cb);
                    });
                }
            });
            async.parallel(tasks, function(err){
                if (err){
                    return next(err);
                }
                instance.values = instance.values.concat(baseInstance.values);
                next(null, instance);
            });
        });
    }

    /**
     * Resolves an inherited multi-join table reference
     * @param attribute
     * @param parentRecordInstance
     * @param connection
     * @param next
     * @returns {*}
     */
    var resolveInheritedRef = function (attribute, parentRecordInstance, options, next) {
        var relationship = attribute.relationship;

        var target = relationship.target;
        if (!target.oneOf) {
            return next('Unknown inherited schema.  Valid values are: oneOf');
        }
        var discriminator = target.discriminator;
        if (!discriminator) {
            return next('A discriminator value is required for an inherited schema');
        }
        var keyBindings = getForeignKeyBindings(attribute);

        var models = {};

        _.forEach(target.oneOf, function (value, key) {
            var model = self.orm.models[value];
            if (model) {
                models[value] = model;
            }
        });
        // Get the base table name
        var baseModel = null;

        _.forEach(models, function (model) {
            var inherits = model.getInherits();
            var tableName = model.getTableName();
            if (inherits && tableName) {
                var base = inherits.schema;
                if (base) {
                    // Should check to see if the baseModel is the same
                    // and also the join clause is the same
                    if (!baseModel) {
                        baseModel = orm.models[base];
                    }
                }
            }
        });
        if (!baseModel) {
            return next('Base model could not be found for inherited schema');
        }
        // Build a base model query
        var baseKeys = {};
        _.forEach(keyBindings, function(keyBinding){
            var superKeyValue = parentRecordInstance.getValue(keyBinding.source);
            baseKeys[keyBinding.source] = superKeyValue;
        });
        var records = [];
        baseModel.findAll(baseKeys, options, function(err, results){
            async.forEachLimit(results, MAX_GETS, function(result, cb){
                var discriminatorValue = result[discriminator];
                var subModel = orm.models[target.oneOf[discriminatorValue]];
                if (!subModel)
                {
                    return cb('Submodel for discriminator: ' + discriminatorValue + ' could not be found');
                }
                var inherits = subModel.getInherits();
                if (!inherits){
                    return cb('Submodel inherits could not be found');
                }
                var baseKeyValue = result[inherits.join.base];
                var subKeyField = inherits.join.subclass;
                var subKeys = {};
                subKeys[subKeyField] = baseKeyValue;
                subModel.findOne(subKeys, options, function(err, subResults){

                    if (err){
                        return cb(err);
                    }
                    if (subResults.length === 1)
                    {
                        // Merge them in with the original result
                        records.push(_.extend(result, subResults[0]));
                    }
                    cb();
                });

            }, function(err){
                next(err, records);
            });
        });
    };

    /**
     * Resolves a simple foreign key reference
     * @param attribute
     * @param parentRecordInstance
     * @param connection
     * @param next
     * @returns {*}
     */
    var resolveSimpleReference = function (attribute, parentRecordInstance, options, next) {
        var relationship = attribute.relationship;
        if (!relationship) {
            return next('Relationship is missing for attribute: ' + attribute.fieldPath);
        }
        var keyBindings = getForeignKeyBindings(attribute);

        var childSchema = orm.models[relationship.target];

        if (!childSchema) {
            return next('Child schema not found for: ' + relationship.target);
        }
        var childQuery = {};
        _.forEach(keyBindings, function (keyBinding) {
            childQuery[keyBinding.target] = parentRecordInstance.getValue(keyBinding.source);
        });

        if (attribute.select)
        {
            _.forEach(attribute.select, function(value, key){
                childQuery[key] = value;
            });
        }

        childSchema.findAll(childQuery, options, function (err, results) {
            if (err) {
                return next(err);
            }
            next(err, results);
        });
    };

    var getForeignKeyBindings = function (attribute) {
        var relationship = attribute.relationship;
        var foreignKey = relationship.foreignKey;

        var keys = [];
        if (foreignKey) {
            if (typeof foreignKey === 'object') {
                if (Array.isArray(foreignKey) === false) {
                    foreignKey = [foreignKey];
                }
                _.forEach(foreignKey, function(fk){
                    if (fk.source && fk.target)
                    {
                        keys.push({
                            source: fk.source,
                            target: fk.target
                        });
                    }
                });
            } else {
                var foreignSource = null;
                var foreignTarget = null;
                if (relationship.type === RELATIONSHIPS.BELONGS_TO) {
                    // Belongs to, keys on the declaring attributes field path, not the primary key
                    foreignSource = attribute.fieldPath;
                    foreignTarget = foreignKey;
                } else {
                    var primKey = self.findPrimaryKeyAttribute();
                    if (primKey) {
                        // This is a simple, HAS_MANY/HAS_ONE, where the source key is the primary key
                        foreignSource = primKey.fieldPath;
                        foreignTarget = foreignKey;
                    }
                }
                if (foreignSource && foreignTarget) {
                    keys.push({
                        source: foreignSource,
                        target: foreignTarget
                    });
                }
            }

        }

        return keys;
    };

    var resolveReference = function (attribute, recordInstance, options, next) {
        var relationship = attribute.relationship;

        if (!relationship) {
            return next('Relationship not found for attribute: ' + attribute.fieldPath);
        }
        var tasks = [];
        var target = relationship.target;
        if (typeof target === 'object') {
            tasks.push(function(cb){
                resolveInheritedRef(attribute, recordInstance, options, cb);
            });
        } else {
            tasks.push(function(cb){
                resolveSimpleReference(attribute, recordInstance, options, cb);
            });
        }
        async.series(tasks, function(err, results){
            if (err) {
                return next(err);
            }
            var refValue = results[0];
            if (relationship.type === RELATIONSHIPS.HAS_ONE) {
                refValue = refValue[0];
            }
            recordInstance.setValue(attribute.fieldPath, refValue);
            next(null, recordInstance);
        });
    };

    this._findAll = function (keys, options, next) {
        var query = {
            where: {}
        };
        query.maxRows = options.maxRows;

        _.forEach(keys, function (value, key) {
            var attribute = self.getAttribute(key);
            if (attribute)
            {
                var column = attribute.column;
                if (column)
                {
                    var valueToQuery = value;
                    if (typeof attribute.queryTranslator === 'function')
                    {
                        valueToQuery = attribute.queryTranslator(value);
                    }
                    query.where[column] = valueToQuery;
                }
            }
        });
        if (!options.connection) {
            return next('Database connection does ot exist');
        }
        options.connection.select(self.getTableName(), query, function (err, results) {
            if (err) {
                return next(err);
            }

            var records = [];
            async.eachSeries(results.rows, function (row, callback) {
                self.build(rawRow(row, results.metaData), {raw: true}, function(err, instance){
                    if (err){
                        return callback(err);
                    }
                    // Go through any references, and resolve them
                    async.eachSeries(self.attributes, function (attribute, cb) {
                        if (attribute.type === FIELD_TYPES.OBJECT)
                        {
                            var relationship = attribute.relationship;
                            if (!relationship || attribute.primaryKey) {
                                return cb();
                            }
                            resolveReference(attribute, instance, options, cb);
                        }else{
                            cb();
                        }

                    }, function (err) {
                        if (err) {
                            return callback(err);
                        }
                        records.push(instance.toJson());
                        callback();
                    });
                });



            }, function (err, record) {
                next(err, records);

            });

        });
    };

    this.findAll = function (keys, options, next) {
        if (next === undefined) {
            next = options;
            options = {};
        }
        options = options || {};
        // Used for infinite loop
        var self = this;

        if (!options.connection) {
            self.orm.runWithConnection(function (connection, done) {
                options.connection = connection;
                self._findAll(keys, options, done);
            }, next);
        } else {
            self._findAll(keys, options, next);
        }
    };

    this.findOne = function (keys, options, next) {
        if (next === undefined)
        {
            next = options;
            options = null;
        }
        this.findAll(keys, options, function (err, records) {
            if (err) {
                return next(err);
            }
            next(null, records);
        });

    };
}

module.exports = ORMModel;