var winston = require('winston');

winston.level = 'debug';

module.exports = require('./lib/orm');
module.exports.FIELD_TYPES = require('./lib/ormConstants').FIELD_TYPES;
module.exports.RELATIONSHIPS = require('./lib/ormConstants').RELATIONSHIPS;